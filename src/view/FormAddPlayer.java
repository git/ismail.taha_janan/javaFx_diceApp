package view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import model.De;
import model.Game;
import model.Player;
import model.Result;
//import javafx.base.F0;

import java.util.List;


public class FormAddPlayer {

    Game currentGame = new Game();
    Result res = new Result();
    De myDe = new De();

    ObservableList<Result> obsList = FXCollections.observableList(currentGame.getlistResults());

    @FXML
    private Label resP1;

    @FXML
    private Label resP2;

    @FXML
    private ListView<Result> resList;


    /*@FXML
    public void clickStartGame(ActiveEvent ev){
        System.err.println("hello");
        res.resPlayer1=myDe.getRandomFace();
        res.resPlayer2=myDe.getRandomFace();
        currentGame.addRes(res);
        //resP1.setText(String.valueOf(res.resPlayer1));
        //resP2.setText(String.valueOf(res.resPlayer2));
        res=new Result();
    }*/

    public void clickStartGame(ActionEvent actionEvent) {
        res.resPlayer1=myDe.getRandomFace();
        res.resPlayer2=myDe.getRandomFace();
        currentGame.addRes(res);
        resP1.textProperty().setValue(String.valueOf(res.resPlayer1));
        resP2.textProperty().setValue(String.valueOf(res.resPlayer2));
        //error here
        //resList.setItems(obsList);
        res=new Result();
    }
}
