import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.layout.StackPane;
import javafx.scene.control.Button;
import model.Game;
import model.Player;
import model.Result;


public class Launcher extends Application {

    public Player player1;
    public Player player2;
    public Game theGame;
    public Result res;

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/FXML/FormAddPlayer.fxml"));

        Scene scene = new Scene(root);
        stage.setTitle("Hello dice game");
        stage.setScene(scene);
        stage.show();
    }

    public void setPlayers(Player p1, Player p2){
        player1 = p1;
        player2 = p2;
    }
}
