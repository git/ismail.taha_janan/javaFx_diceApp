package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Game {
    private List<Result> results = new ArrayList<Result>();

    public void addRes(Result result) {
        results.add(result);
    }

    public List<Result> getlistResults() {
        return Collections.unmodifiableList(results);
    }
}
